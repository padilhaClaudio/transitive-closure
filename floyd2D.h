#ifndef __floyd2D_
#define __floyd2D_

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>

// used for each process to cumpute its share of the data locally
void computeLocalMatrix (int ** localMat, int * linha, int * coluna, int n);

// Process 0 sends other processes their share of the matrix
void sendLocalMatrix (int ** graph, int k, int num_procs, int dest_rank);

// Floyd parallel function. After this function each process has is local matrix computed
void floyd2DPipeline (FILE * f, int k, int rank, int size);

// Read data and return a matrix representing the graph
int ** readData (FILE * f, int k);

#endif
