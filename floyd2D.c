#include "floyd2D.h"

#define min(current,new) (current<new?current:new)

int ** readData (FILE * f, int k)
{
    int ** ret = (int**) malloc(k * sizeof(int *));

    for (int i = 0; i < k; i++)
        ret[i] = (int *) malloc(k * sizeof(int));

    for (int i = 0; i < k; i++)
    {
        for (int j =0; j < k; j++)
        {
            ret[i][j] = fscanf(f, "%d", &(ret[i][j]) );
        }
    }

    fclose(f);

    return ret;
}

void computeLocalMatrix (int ** localMat, int * linha, int * coluna, int tam)
{
    for (int i = 0; i < tam; i ++)
    {
        for (int j = 0; j < tam; j++)
        {
            int current = localMat[i][j];
            int new = linha[j] + coluna[j];

            localMat[i][j] = min(current,new);
        }
    }
}

void sendLocalMatrix (int ** graph, int k,  int num_procs, int dest_rank) 
{
    int raiz_p = sqrt(num_procs);
    int matDim = k/raiz_p;

    int * data = (int *)malloc( (k * k / num_procs) * sizeof(int));

    int l = 0;

    for (int i = (int)(dest_rank/raiz_p) * matDim; i < matDim; i++)
    {
        for (int j = (dest_rank%raiz_p) * matDim; j < matDim; j++)
        {
            data[l++] = graph[i][j];  
        }
    }

    int **array= (int **)malloc(matDim * sizeof(int*));
    for (int i=0; i < matDim; i++)
        array[i] = &(data[raiz_p*i]);

    MPI_Send(&(array[0][0]), (k * k / num_procs) , MPI_INT, dest_rank, 0, MPI_COMM_WORLD);

    free(array[0]);
    free(array);
}


void floyd2DPipeline (FILE * f, int k, int rank, int size)
{

    MPI_Status status;
    
    int raiz_p = sqrt(size);
    int locMatDim = k/raiz_p;

    int index[2];
    index[0]= rank/raiz_p;
    index[1] = rank % raiz_p;

    int ** localMat = (int **) malloc(locMatDim * sizeof(int*));


    int * col = (int *) malloc(locMatDim * sizeof(int));
    int * ln = (int *) malloc(locMatDim * sizeof(int));

    // Dividing initial matrix
    if (rank == 0)
    {

        int ** graph = readData(f, k); 

        for (int j = 1; j < size; j++)
            sendLocalMatrix(graph, k,  size, j);                        // Send each process their share of the data

        for (int i = 0; i < locMatDim; i++)
            localMat[i] = (int *) malloc(locMatDim * sizeof(int));

         for (int i = 0; i < locMatDim; i++)
        {
            for (int j = 0; j < locMatDim; j++)
            {
                localMat[i][j] = graph[i][j];                       // Process 0 gets its share of the data
            }
        }
    }else 
    {
        MPI_Recv(&(localMat[0][0]), k*k/size, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    }

    // Calculating transitive closure
    for (int i = 0; i < k; i++)
    {
        if (index[0] == i / locMatDim)             // If I have a share of ith line, I send it
        {
           int * linha = malloc(locMatDim * sizeof(int));

           for (int j = 0; j < locMatDim; j++)
           {
               linha[j] = localMat[i][j];
           }

           if (index[0] > 0)                // Send line up
           {
               MPI_Send(linha, locMatDim, MPI_INT, rank - raiz_p, rank, MPI_COMM_WORLD);
           }

           if (index[0] < (raiz_p -1) )     // Send line down
           {
               MPI_Send(linha, locMatDim, MPI_INT, rank + raiz_p, rank, MPI_COMM_WORLD);
           }

           if (index[1] > 0)                // Send line left
           {
               MPI_Send(linha, locMatDim, MPI_INT, rank - 1, rank, MPI_COMM_WORLD);
           } 

           if (index[1] < raiz_p - 1)       // Send line right
           {
               MPI_Send(linha, locMatDim, MPI_INT, rank + 1, rank, MPI_COMM_WORLD);
           }

           free(linha);
        }
        if (index[1] == i / locMatDim)                     // If I hava a share of ith column, I send it
        {
            int * coluna = malloc(locMatDim * sizeof(int));

            for (int j = 0; j < locMatDim; j++)
            {
                coluna[j] = localMat[j][i];
            }

            if (index[0] > 0)                // Send column up
           {
               MPI_Send(coluna, locMatDim, MPI_INT, rank - raiz_p, rank, MPI_COMM_WORLD);
           }

           if (index[0] < (raiz_p -1) )     // Send column down
           {
               MPI_Send(coluna, locMatDim, MPI_INT, rank + raiz_p, rank, MPI_COMM_WORLD);
           }

           if (index[1] > 0)                // Send column left
           {
               MPI_Send(coluna, locMatDim, MPI_INT, rank - 1, rank, MPI_COMM_WORLD);
           } 

           if (index[1] < raiz_p - 1)       // Send column right
           {
               MPI_Send(coluna, locMatDim, MPI_INT, rank + 1, rank, MPI_COMM_WORLD);
           }

            free(coluna);
        }

        if  (index[0] != i / locMatDim && index[1] != i / locMatDim)                                                    // Receives column and line and foward them
        {
            MPI_Recv(ln, locMatDim, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            if (status.MPI_SOURCE == rank - 1 && index[1] < raiz_p - 1)                       // Received from left
            {
                MPI_Send(ln, locMatDim, MPI_INT, rank + 1, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank + 1 && index[1] > 0)                            // Received from right
            {
                MPI_Send(ln, locMatDim, MPI_INT, rank - 1, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank - locMatDim && index[0] < raiz_p - 1)           // Received form above
            {
                MPI_Send(ln, locMatDim, MPI_INT, rank + locMatDim, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank + locMatDim && index[0] > 0)                     // Received from below
            {   
                MPI_Send(ln, locMatDim, MPI_INT, rank - locMatDim, rank, MPI_COMM_WORLD);
            }

            MPI_Recv(col, locMatDim, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            if (status.MPI_SOURCE == rank - 1 && index[1] < raiz_p - 1)                       // Received from left
            {
                MPI_Send(col, locMatDim, MPI_INT, rank + 1, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank + 1 && index[1] > 0)                            // Received from right
            {
                MPI_Send(col, locMatDim, MPI_INT, rank - 1, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank - locMatDim && index[0] < raiz_p - 1)           // Received form above
            {
                MPI_Send(col, locMatDim, MPI_INT, rank + locMatDim, rank, MPI_COMM_WORLD);
            }
            else if (status.MPI_SOURCE == rank + locMatDim && index[0] > 0)                     // Received from below
            {   
                MPI_Send(col, locMatDim, MPI_INT, rank - locMatDim, rank, MPI_COMM_WORLD);
            }


            computeLocalMatrix(localMat, ln, col, locMatDim);
        }
    }
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int num_procs, my_id;
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);

    FILE * f = fopen(argv[1], "r");


    int k;
    fscanf(f, "%d", &k);

    floyd2DPipeline(f, k, my_id, num_procs);

    MPI_Finalize();
}